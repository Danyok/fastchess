<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1">
<context>
    <name>About</name>
    <message>
        <source>Back to settings</source>
        <translation>Назад в настройки</translation>
    </message>
    <message>
        <source>About page</source>
        <translation>О приложении</translation>
    </message>
    <message>
        <source>Fast chess, version</source>
        <extracomment>The name of the app followed with a version number</extracomment>
        <translation>Блиц шахматы, версия</translation>
    </message>
    <message>
        <source>Translations</source>
        <translation>Переводы</translation>
    </message>
    <message>
        <source>German (Nit)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Spanish (Carlos Gonzalez)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Finnish (Riku Lahtinen)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>French (lutinotmalin)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Hungarian (leoka)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Dutch (Heimen Stoffels)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Polish (szopin)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Chinese (China) (Historyscholar)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Contributions</source>
        <translation>Содействия</translation>
    </message>
    <message>
        <source>The design of the pieces (Kapu)</source>
        <translation>Дизайнер фигур (Kapu)</translation>
    </message>
    <message>
        <source>Third party software</source>
        <translation>Вспомогательные компоненты</translation>
    </message>
    <message>
        <source>Stockfish engine, v5 (stockfishchess.org)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Licence</source>
        <translation>Лицензия</translation>
    </message>
    <message>
        <source>Copyright (c) 2015, Riku Lahtinen</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Licensed under GPLv3. License, source code and more information:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Dutch (Belgium) (Nathan Follens)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Russian (Danyok)</source>
        <translation>Русский (Danyok)</translation>
    </message>
</context>
<context>
    <name>Boardview</name>
    <message>
        <source>Settings</source>
        <translation>Настройки</translation>
    </message>
    <message>
        <source>Save and manage games</source>
        <translation>Сохранение и управление играми</translation>
    </message>
    <message>
        <source>Show moves</source>
        <translation>Показать ходы</translation>
    </message>
    <message>
        <source>Check!</source>
        <translation>Шах!</translation>
    </message>
    <message>
        <source>Checkmate!</source>
        <translation>Мат!</translation>
    </message>
    <message>
        <source>Stalemate!</source>
        <translation>Пат!</translation>
    </message>
    <message>
        <source>White won!</source>
        <translation>Белые выиграли!</translation>
    </message>
    <message>
        <source>Black won!</source>
        <translation>Чёрные выиграли!</translation>
    </message>
    <message>
        <source>Pause</source>
        <translation>Пауза</translation>
    </message>
    <message>
        <source>Continue</source>
        <translation>Продолжить</translation>
    </message>
    <message>
        <source>Reset</source>
        <translation>Cброс</translation>
    </message>
    <message>
        <source>Chess board</source>
        <translation>Шахматная доска</translation>
    </message>
    <message>
        <source>min:s</source>
        <translation>мин:c</translation>
    </message>
    <message>
        <source>Start</source>
        <translation>Начало</translation>
    </message>
</context>
<context>
    <name>Clockview</name>
    <message>
        <source>Black´s turn</source>
        <translation>Xод чёрных</translation>
    </message>
    <message>
        <source>White´s turn</source>
        <translation>Xод белых</translation>
    </message>
    <message>
        <source>Pause</source>
        <translation>Пауза</translation>
    </message>
    <message>
        <source>Continue</source>
        <translation>Продолжить</translation>
    </message>
    <message>
        <source>Reset</source>
        <translation>Cброс</translation>
    </message>
    <message>
        <source>Settings</source>
        <translation>Настройки</translation>
    </message>
    <message>
        <source>Chess clock</source>
        <translation>Шахматные часы</translation>
    </message>
    <message>
        <source>min:s</source>
        <extracomment>Minutes:seconds</extracomment>
        <translation>мин:c</translation>
    </message>
    <message>
        <source>White´s clock</source>
        <translation>Tаймер белых</translation>
    </message>
    <message>
        <source>Black´s clock</source>
        <translation>Tаймер чёрных</translation>
    </message>
</context>
<context>
    <name>Connbox</name>
    <message>
        <source>TCP connection</source>
        <translation>TCP подключение</translation>
    </message>
    <message>
        <source>My device info</source>
        <translation>Моё устройство</translation>
    </message>
    <message>
        <source>Opponent&apos;s device info</source>
        <translation>Устройство оппонента</translation>
    </message>
    <message>
        <source>Test connection</source>
        <translation>Тестовое подключение</translation>
    </message>
    <message>
        <source>Color mismatch, change my color</source>
        <extracomment>Reports the player that colors selected in games in different devices prevent the game start. Another player has to change the color the game to proceed.</extracomment>
        <translation>Сменить цвет</translation>
    </message>
    <message>
        <source>Reset</source>
        <translation>Cброс</translation>
    </message>
    <message>
        <source>Start</source>
        <translation>Начало</translation>
    </message>
    <message>
        <source>Test in progress</source>
        <translation>Тестирование</translation>
    </message>
    <message>
        <source>Waiting your opponent to start</source>
        <translation>Ожидание оппонента</translation>
    </message>
    <message>
        <source>Retest the connection</source>
        <translation>Ретест соединения</translation>
    </message>
    <message>
        <source>IP address</source>
        <translation>IP адрес</translation>
    </message>
    <message>
        <source>Port number</source>
        <translation>Номер порта</translation>
    </message>
    <message>
        <source>Settings</source>
        <translation>Настройки</translation>
    </message>
</context>
<context>
    <name>CoverPage</name>
    <message>
        <source>Fast chess</source>
        <translation>Блиц шахматы</translation>
    </message>
</context>
<context>
    <name>GameInfo</name>
    <message>
        <source>Tag pairs</source>
        <extracomment>&quot;Tag pairs&quot; could be also e.g. &quot;Game info&quot;. This section of the page has the general information of the game. See more https://en.wikipedia.org/wiki/Portable_Game_Notation</extracomment>
        <translation>Информация об игре</translation>
    </message>
    <message>
        <source>Movetext, Coordinate notation</source>
        <extracomment>&quot;Movetext, Coordinate notation&quot; could be also e.g. &quot;Moves in coordinates&quot;. See more https://en.wikipedia.org/wiki/Portable_Game_Notation</extracomment>
        <translation>Список ходов и координат</translation>
    </message>
    <message>
        <source>Game</source>
        <translation>Игра</translation>
    </message>
</context>
<context>
    <name>GameInfo2</name>
    <message>
        <source>Current game</source>
        <translation>Текущая игра</translation>
    </message>
    <message>
        <source>Tag pairs</source>
        <extracomment>&quot;Tag pairs&quot; could be also &quot;Game info&quot;. This section of the page has the general information of the game. See more https://en.wikipedia.org/wiki/Portable_Game_Notation</extracomment>
        <translation>Информация об игре</translation>
    </message>
    <message>
        <source>Movetext, Coordinate notation</source>
        <extracomment>&quot;Movetext, Coordinate notation&quot; could be also e.g. &quot;Moves in coordinates&quot;. See more https://en.wikipedia.org/wiki/Portable_Game_Notation</extracomment>
        <translation>Список ходов и координат</translation>
    </message>
</context>
<context>
    <name>GameList</name>
    <message>
        <source>Save current game</source>
        <translation>Сохранить текущую игру</translation>
    </message>
    <message>
        <source>Game list</source>
        <translation>Список игры</translation>
    </message>
    <message>
        <source>Game</source>
        <translation>Игра</translation>
    </message>
    <message>
        <source>Show game</source>
        <translation>Показать игру</translation>
    </message>
    <message>
        <source>Delete game</source>
        <translation>Удалить игру</translation>
    </message>
    <message>
        <source>Animate game</source>
        <translation>Показать игру</translation>
    </message>
</context>
<context>
    <name>GameSelector</name>
    <message>
        <source>Game</source>
        <translation>Игра</translation>
    </message>
    <message>
        <source>Select game</source>
        <translation>Выбор игры</translation>
    </message>
</context>
<context>
    <name>Settings</name>
    <message>
        <source>Save settings</source>
        <translation>Сохранить настройки</translation>
    </message>
    <message>
        <source>Play chess</source>
        <translation>Играть в шахматы</translation>
    </message>
    <message>
        <source>Clock view</source>
        <translation>Шахматные часы</translation>
    </message>
    <message>
        <source>About</source>
        <translation>О пpoгpаммe</translation>
    </message>
    <message>
        <source>-1 min</source>
        <extracomment>Reduce time by 1 minute</extracomment>
        <translation>-1 мин</translation>
    </message>
    <message>
        <source>+1 min</source>
        <extracomment>Increase time by 1 minute</extracomment>
        <translation>+1 мин</translation>
    </message>
    <message>
        <source>-1 s</source>
        <extracomment>Reduce time by 1 second</extracomment>
        <translation>-1 с</translation>
    </message>
    <message>
        <source>+1 s</source>
        <extracomment>Increase time by 1 second</extracomment>
        <translation>+1 с</translation>
    </message>
    <message>
        <source>min</source>
        <extracomment>min is an abbreviation of a minute</extracomment>
        <translation>мин</translation>
    </message>
    <message>
        <source>s</source>
        <extracomment>s is an abbreviation of a second</extracomment>
        <translation>c</translation>
    </message>
    <message>
        <source>Settings page</source>
        <translation>Страница настроек</translation>
    </message>
    <message>
        <source>Clock settings</source>
        <translation>Настройки часов</translation>
    </message>
    <message>
        <source>White</source>
        <translation>Белый</translation>
    </message>
    <message>
        <source>Black</source>
        <translation>Черный</translation>
    </message>
    <message>
        <source>Increment/move</source>
        <extracomment>Sets how many seconds are added to the total remaining time of the game per move.</extracomment>
        <translation>Прибавка за ход</translation>
    </message>
    <message>
        <source>Time counting</source>
        <extracomment>The time is counted upwards from zero to max or downwards from max to zero. This is label for that ComboBox.</extracomment>
        <translation>Подсчёт времени</translation>
    </message>
    <message>
        <source>Downwards</source>
        <translation>Убывание</translation>
    </message>
    <message>
        <source>Upwards</source>
        <translation>Возрастание</translation>
    </message>
    <message>
        <source>Chess settings</source>
        <translation>Настройки шахмате</translation>
    </message>
    <message>
        <source>Opponent</source>
        <translation>Оппонент</translation>
    </message>
    <message>
        <source>Stockfish</source>
        <extracomment>Stockfish is a name of the chess engine, more info https://stockfishchess.org/</extracomment>
        <translation>Stockfish</translation>
    </message>
    <message>
        <source>Human</source>
        <translation>Человек</translation>
    </message>
    <message>
        <source>Another device</source>
        <translation>Другое устройство</translation>
    </message>
    <message>
        <source>Port number</source>
        <translation>Номер порта</translation>
    </message>
    <message>
        <source>Random</source>
        <translation>Случайный</translation>
    </message>
    <message>
        <source>Fixed</source>
        <translation>Фиксированный</translation>
    </message>
    <message>
        <source>My color</source>
        <translation>Мой цвет</translation>
    </message>
    <message>
        <source>Opening</source>
        <translation>Начало</translation>
    </message>
    <message>
        <source>Saved game</source>
        <translation>Сохранённая игра</translation>
    </message>
    <message>
        <source>Skill Level</source>
        <translation>Уровень квалификации</translation>
    </message>
    <message>
        <source>Movetime</source>
        <extracomment>Sets the time the Stockfish engine has per move.</extracomment>
        <translation>Время на ход</translation>
    </message>
    <message>
        <source>View settings</source>
        <translation>Настройки отображения</translation>
    </message>
    <message>
        <source>Default view</source>
        <translation>Начальный экран</translation>
    </message>
    <message>
        <source>Chess clock</source>
        <translation>Шахматные часы</translation>
    </message>
    <message>
        <source>Chess board</source>
        <translation>Шахматная доска</translation>
    </message>
    <message>
        <source>Unlike</source>
        <extracomment>The style of the pieces is unlike</extracomment>
        <translation>Упрощённый</translation>
    </message>
    <message>
        <source>Classic</source>
        <extracomment>The style of the pieces is classic</extracomment>
        <translation>Классический</translation>
    </message>
    <message>
        <source>Style of the pieces</source>
        <extracomment>The style of the pieces selector</extracomment>
        <translation>Стиль фигур</translation>
    </message>
    <message>
        <source>Personal art</source>
        <extracomment>Player can select the pieces of her or his choice</extracomment>
        <translation>Собственный стиль</translation>
    </message>
</context>
<context>
    <name>Settings_dialog_personal_art</name>
    <message>
        <source>Files required</source>
        <translation>Требуемые файлы</translation>
    </message>
    <message>
        <source>Testing</source>
        <translation>Тестирование</translation>
    </message>
    <message>
        <source>Current folder path</source>
        <translation>Текущий путь</translation>
    </message>
    <message>
        <source>If the current folder path is already right, please cancel this operation.</source>
        <translation>Если текущий путь верен, то отмените эту операцию.</translation>
    </message>
    <message>
        <source>File selection</source>
        <translation>Выбор файла</translation>
    </message>
    <message>
        <source>Your personal art folder should have all the image files as follows:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>For the start of the testing of this feature you can copy the required files from the device path:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Selecting only one image file from your personal art folder is required.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>funktiot</name>
    <message>
        <source>Reset</source>
        <translation>Cброс</translation>
    </message>
</context>
<context>
    <name>harbour-shakkikello</name>
    <message>
        <source>Start</source>
        <translation>Начать</translation>
    </message>
    <message>
        <source>Settings</source>
        <translation>Настройки</translation>
    </message>
    <message>
        <source>Downwards</source>
        <translation>Убывание</translation>
    </message>
</context>
</TS>
